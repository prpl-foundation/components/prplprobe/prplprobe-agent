#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}
name="datacollect-agent"

case $1 in
    start|boot)
        source /etc/environment
        if [ -s /var/run/${name}.pid ] && [ -d "/proc/$(cat /var/run/${name}.pid)/fdinfo" ]; then
           echo "datacollect-agent is already started"
           exit 0
        fi
        datacollect-agent -D
        ;;
    stop|shutdown)
        if [ -f /var/run/${name}.pid ]; then
            kill `cat /var/run/${name}.pid`
        else
            killall ${name}
        fi
        ;;
    debuginfo)
        ubus-cli "DataCollect.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    fail)
        ;;
    log)
        echo "TODO log datacollect-agent client"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac

