/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__AGENT_COLLECTOR_H__)
#define __AGENT_COLLECTOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "agent.h"
#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>

#include <amxc/amxc_string.h>
#include <amxc/amxc_variant_type.h>

#include <amxj/amxj_variant.h>

#include <amxd/amxd_common.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_parameter.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_transaction.h>

#include <debug/sahtrace.h>

#define LCM_CONTAINER_PATH "/lcm/cthulhu/rootfs/"

#define DATACOLLECT_AHDSOURCE_PATH "DataCollect.AhDSource"

typedef struct _gen_ts_stats {
    uint32_t bytes_sent;
    uint32_t msg_sent;
} gen_ts_stats;

typedef struct _day_ts_stats {
    uint32_t bytes_sent;
    uint32_t msg_sent;
    uint32_t min_bytes_sent_persec;
    uint32_t min_bytes_sent_permin;
    uint32_t min_bytes_sent_perhr;
    uint32_t max_bytes_sent_persec;
    uint32_t max_bytes_sent_permin;
    uint32_t max_bytes_sent_perhr;
    uint32_t min_msg_sent_persec;
    uint32_t min_msg_sent_permin;
    uint32_t min_msg_sent_perhr;
    uint32_t max_msg_sent_persec;
    uint32_t max_msg_sent_permin;
    uint32_t max_msg_sent_perhr;
    double avg_bytes_sec_ts;
    double avg_bytes_min_ts;
    double avg_bytes_hr_ts;
    double avg_msg_sec_ts;
    double avg_msg_min_ts;
    double avg_msg_hr_ts;
} day_ts_stats;

typedef struct _sec_stats {
    gen_ts_stats ts[5];
} sec_stats_t;

typedef struct _min_stats {
    gen_ts_stats ts[10];
} min_stats_t;

typedef struct _hr_stats {
    gen_ts_stats ts[24];
} hr_stats_t;

typedef struct _day_stats {
    day_ts_stats ts[10];
} day_stats_t;

typedef struct _collector_conf {
    char* name;
    char* uuid;
    int pos; // DM position
    amxd_object_t* agent_obj;
    amxd_object_t* ahdsource_obj;
    amxd_object_t* ahdcpmessage_obj;
    amxd_object_t* ahdcpmessage_config_obj;
    amxd_object_t* ahdcpfile_obj;
    amxd_object_t* ahdcpfile_config_obj;
    // message-based ahdcp
    char* msg_ahdcp_name;
    bool msg_auto_restart;
    int64_t max_bytes_per_sec;
    int64_t max_bytes_per_min;
    int64_t max_bytes_per_hr;
    int64_t max_bytes_per_day;
    int64_t cur_bytes_per_sec;
    int64_t cur_bytes_per_min;
    int64_t cur_bytes_per_hr;
    int64_t cur_bytes_per_day;
    int64_t cur_msg_sent_per_sec;
    int64_t cur_msg_sent_per_min;
    int64_t cur_msg_sent_per_hr;
    int64_t cur_msg_sent_per_day;
    // file-based ahdcp
    amxp_timer_t* file_send_timer;
    amxp_timer_t* file_wait_timer;
    char* file_ahdcp_name;
    char* file_ahdcp_url;
    char* file_ahdcp_crt_name;
    bool file_auto_restart;
    int64_t file_max_allowed_size;
    int64_t file_send_interval;
    int64_t file_wait_interval;
    int64_t file_cur_size;
    int64_t file_num_sent;
    // stats
    sec_stats_t stats_per_sec;
    min_stats_t stats_per_min;
    hr_stats_t stats_per_hr;
    day_stats_t stats_per_day;
    // stats analysis
    uint32_t min_bytes_sent_perhr;
    uint32_t min_bytes_sent_permin;
    uint32_t min_bytes_sent_persec;
    uint32_t max_bytes_sent_perhr;
    uint32_t max_bytes_sent_permin;
    uint32_t max_bytes_sent_persec;
    uint32_t min_msg_sent_perhr;
    uint32_t min_msg_sent_permin;
    uint32_t min_msg_sent_persec;
    uint32_t max_msg_sent_perhr;
    uint32_t max_msg_sent_permin;
    uint32_t max_msg_sent_persec;
    uint32_t num_sec_ts;
    uint32_t num_min_ts;
    uint32_t num_hr_ts;
    double avg_bytes_sec_ts;
    double avg_bytes_min_ts;
    double avg_bytes_hr_ts;
    double avg_msg_sec_ts;
    double avg_msg_min_ts;
    double avg_msg_hr_ts;

    amxc_llist_it_t it;
} col_conf_t;


void init_col_lst(void);
amxc_llist_t* get_col_lst(void);
void register_col(const char* uuid);
uint32_t msg_based_update(col_conf_t* col, uint32_t size);
void col_update_day_stats(amxp_timer_t* timer, void* priv UNUSED);
void col_update_hr_stats(amxp_timer_t* timer, void* priv UNUSED);
void col_update_min_stats(amxp_timer_t* timer, void* priv UNUSED);
void col_update_sec_stats(amxp_timer_t* timer, void* priv UNUSED);
bool agent_get_ahdcpmessage_enables(col_conf_t* col);

static inline int32_t agent_dm_get_ahdcpmessage_config_param(col_conf_t* col, const char* param) {
    return amxd_object_get_value(int32_t, col->ahdcpmessage_config_obj, param, NULL);
}

static inline int32_t agent_dm_get_ahdcpfile_config_param(col_conf_t* col, const char* param) {
    return amxd_object_get_value(int32_t, col->ahdcpfile_config_obj, param, NULL);
}


#ifdef __cplusplus
}
#endif

#endif // __AGENT_COLLECTOR_H__
