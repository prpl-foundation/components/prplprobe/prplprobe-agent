# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.7.0 - 2024-12-20(15:27:21 +0000)

### Other

- - [Prpl] - Uncrustify code
- - [Prpl][agent] - set and get data model entries and
- - [Prpl][Agent] - "No limitation" volumetry feature and

## Release v1.6.0 - 2024-12-06(14:41:35 +0000)

### Other

- - [Prpl][agent] - write messages to file and send

## Release v1.5.0 - 2024-11-05(16:08:54 +0000)

### Other

- - [Prpl][agent] - check volumetry of collector

## Release v1.4.1 - 2024-10-25(19:04:52 +0000)

## Release v1.4.0 - 2024-10-25(18:43:32 +0000)

### Other

- - [Prpl] [agent] - Read config file and add new

## Release v1.2.0 - 2024-08-09(13:33:12 +0000)

### Other

- - [Prpl][Lib] - New lib functions

## Release v1.1.0 - 2024-07-26(12:08:11 +0000)

### Other

- - [Prpl] [Agent] - retrieve device informations

## Release v1.0.0 - 2024-07-25(08:50:46 +0000)

### Other

- - [Prpl] [Agent] - Create ambiorix plugin

## Release v0.0.21 - 2023-11-02(07:19:48 +0000)

### Other

- - [Eyes'ON][prpl] Document Eyes'ON Prpl APIs

## Release v0.0.20 - 2023-10-17(07:48:27 +0000)

### Other

- - [Prpl] Compile containers with ??? version of LCM SDK for Network X

## Release v0.0.19 - 2023-10-09(14:51:39 +0000)

### Other

- - [Prpl] Open source delivery process

## Release v0.0.18 - 2023-10-06(11:20:17 +0000)

### Other

- Opensource Component

## Release v0.0.17 - 2023-10-04(15:17:19 +0000)

### Other

- - [Prpl] Compile containers with ??? version of LCM SDK for Network X

## Release v0.0.16 - 2023-09-26(14:46:18 +0000)

### Other

- - [Prpl] Rename all "agent" into "prplprobe agent"

## Release v0.0.15 - 2023-09-19(08:46:04 +0000)

### Other

- - [Eyes'ON][prpl] Add CI tests on new Eyes'ON prpl components
- - [Prpl] Rename all "agent" into "prplprobe agent"
- - Use port 443 for MQTT data sending on FUT/generic server

## Release v0.0.14 - 2023-08-28(09:13:22 +0000)

### Other

- - [Eyes'ON][prpl] Enable core dump save

## Release v0.0.13 - 2023-08-25(12:58:28 +0000)

### Other

- - [Eyes'ON][prpl] Add a client lib to have common code and simplify integration of new modules
- - [Eyes'ON][prpl] Make SAH CI work on agent and modules
- - [Prpl] Package for open source delivery

## Release v0.0.12 - 2023-06-06(15:19:07 +0000)

### Other

- - [USP][MQTT] Missing unique keys for MQTT data model

## Release v0.0.11 - 2023-05-02(18:20:44 +0000)

### Other

- - gRPC clean integration

## Release v0.0.10 - 2023-04-07(13:20:20 +0000)

### Other

- - [Eyes'ON][prpl] Integrate Eyes'ON agent + modules in LCM

## Release v0.0.9 - 2023-04-07(09:25:45 +0000)

### Other

- - [STEP3][TR-181 Proxy] Implement gRPC <--> AMX API proxy (DM + necessary RPCs)

## Release v0.0.8 - 2023-03-10(10:03:49 +0000)

### Other

- - [eyeson] Crash of agent on hgw reboot

## Release v0.0.7 - 2023-03-08(09:46:21 +0000)

### Other

- - Use only libsahtrace for logging messages

## Release v0.0.6 - 2023-02-23(15:46:37 +0000)

### Other

- - [Eyes'ON][prpl][agent] Agent crashes when receiving events

## Release v0.0.5 - 2023-02-21(11:13:34 +0000)

## Release v0.0.4 - 2023-02-20(17:37:26 +0000)

### Other

- - Send event with tr181-mqtt

## Release v0.0.3 - 2023-02-07(10:14:06 +0000)

### Other

- - Define mandatory Cloudevent header attributes
- - [Eyes'ON][Prpl] Some devices are missing deviceinfo attributes
- - [Eyes'ON][Prpl] Update test_prpl keyspace to figaro

## Release v0.0.2 - 2022-12-20(14:37:15 +0000)

### Other

- - [Prpl] Clean and merge all devs that are in branch

## Release v0.0.1 - 2022-12-09(15:34:44 +0000)

### Other

- - [Prpl] Clean and merge all devs that are in branch

