/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "agent_file.h"

#define BASE_PATH "/"
#define FILE_NAME "datacollect_file.txt"
#define CRT_PATH "/etc/config/datacollect/certs/"

#define ME "agent_ftx"

static const cstring_t ftx_get_request_type_string(uint32_t type) {
    switch(type) {
    case ftx_request_type_upload: return "upload";
    case ftx_request_type_download: return "download";
    case ftx_request_type_info: return "info";
    default: break;
    }
    return "undefined";
}

static bool agent_file_transfer_cb(ftx_request_t* req, void* userdata) {
    SAH_TRACEZ_INFO(ME, "File transfer callback");
    ftx_error_code_t error_code;
    amxc_ts_t* ts_req;
    amxc_ts_t* ts_start;
    amxc_ts_t* ts_end;
    char time_req_str[36] = {0};
    char time_start_str[36] = {0};
    char time_end_str[36] = {0};
    int64_t wait_nsec;
    int64_t tx_nsec;

    if(!req) {
        SAH_TRACEZ_ERROR(ME, "\n%s req invalid\n", __FUNCTION__);
        return true;
    }

    error_code = ftx_request_get_error_code(req);
    ts_req = ftx_request_get_request_time(req);
    ts_start = ftx_request_get_start_time(req);
    ts_end = ftx_request_get_end_time(req);

    amxc_ts_format(ts_req, time_req_str, sizeof(time_req_str));
    amxc_ts_format(ts_start, time_start_str, sizeof(time_start_str));
    amxc_ts_format(ts_end, time_end_str, sizeof(time_end_str));

    wait_nsec = (ts_start->sec - ts_req->sec) * 1000000000 + (ts_start->nsec - ts_req->nsec);
    tx_nsec = (ts_end->sec - ts_start->sec) * 1000000000 + (ts_end->nsec - ts_start->nsec);

    SAH_TRACEZ_INFO(ME, "\n%s >>>>>>>>>>>>>>>>>\n", __FUNCTION__);
    SAH_TRACEZ_INFO(ME, "%s request type     : %u (%s)\n", __FUNCTION__, ftx_request_get_type(req), ftx_get_request_type_string(ftx_request_get_type(req)));
    SAH_TRACEZ_INFO(ME, "%s error code       : %u (%s)\n", __FUNCTION__, error_code, (error_code == ftx_error_code_no_error) ? "success" : "failure");
    SAH_TRACEZ_INFO(ME, "%s content type     : %s\n", __FUNCTION__, ftx_request_get_content_type(req));
    SAH_TRACEZ_INFO(ME, "%s file size bytes  : %u\n", __FUNCTION__, ftx_request_get_file_size(req));
    SAH_TRACEZ_INFO(ME, "%s error reason     : %s\n", __FUNCTION__, ftx_request_get_error_reason(req));
    SAH_TRACEZ_INFO(ME, "%s time req         : %s\n", __FUNCTION__, time_req_str);
    SAH_TRACEZ_INFO(ME, "%s time start       : %s\n", __FUNCTION__, time_start_str);
    SAH_TRACEZ_INFO(ME, "%s time end         : %s\n", __FUNCTION__, time_end_str);
    SAH_TRACEZ_INFO(ME, "%s user data        : %s\n", __FUNCTION__, userdata ? (char*) userdata : "");
    SAH_TRACEZ_INFO(ME, "%s duration wait ms : " "%" PRId64 "\n", __FUNCTION__, wait_nsec / 1000000);
    SAH_TRACEZ_INFO(ME, "%s duration tx ms   : " "%" PRId64 "\n", __FUNCTION__, tx_nsec / 1000000);
    SAH_TRACEZ_INFO(ME, "%s <<<<<<<<<<<<<<<<<\n", __FUNCTION__);


    char* file_path = (char*) userdata;
    // remove file
    if(remove(file_path) == 0) {
        SAH_TRACEZ_INFO(ME, "File removed successfully");
    } else {
        SAH_TRACEZ_INFO(ME, "Unable to remove file");
    }
    free(file_path);

    ftx_request_delete(&req);
    return true;
}

void agent_file_transfer(col_conf_t* col) {
    SAH_TRACEZ_INFO(ME, "Col %s: File transfer", col->name);

    //init size
    col->file_cur_size = 0;
    col->file_num_sent++;

    ftx_request_t* request = NULL;

    char username[] = ""; // not used for now
    char password[] = "";

    char file_name[128] = "datacollect.txt";
    char old_file_name[512];
    char new_file_name[512];

    sprintf(old_file_name, "%s/%s/%s/%s", LCM_CONTAINER_PATH, col->uuid, BASE_PATH, FILE_NAME);
    const char* dev_sn = get_deviceinfo_serialnumber();
    if(dev_sn && strcmp(dev_sn, "")) {   // if different of empty string
        time_t now = time(NULL);
        sprintf(file_name, "datacollect_%s_%s_%lu.txt", dev_sn, col->uuid, (unsigned long) now);

        // add SN, UUID and current timestamp to file name
        sprintf(new_file_name, "%s/%s/%s/%s", LCM_CONTAINER_PATH, col->uuid, BASE_PATH, file_name);
        if(rename(old_file_name, new_file_name) == 0) {
            SAH_TRACEZ_INFO(ME, "File renamed successfully to %s", new_file_name);
        } else {
            SAH_TRACEZ_INFO(ME, "Unable to rename file");
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Problem with SerialNumber");
    }

    if(col->file_ahdcp_url) {
        when_false_trace((strlen(col->file_ahdcp_url) < 2048), fail, ERROR, "URL MaximumSize is 2048");
    }
    when_false_trace((strlen(username) < 256), fail, ERROR, "Username MaximumSize is 256");
    when_false_trace((strlen(password) < 256), fail, ERROR, "Password MaximumSize is 256");

    when_failed_trace(ftx_request_new(&request, ftx_request_type_upload, agent_file_transfer_cb), fail, ERROR, "Could not allocate memory.");
    if(col->file_ahdcp_url) {
        when_failed_trace(ftx_request_set_url(request, col->file_ahdcp_url), fail, ERROR, "URL does not set.");
    }
    when_failed_trace(ftx_request_set_credentials(request, ftx_authentication_any, username, password), fail, ERROR, "Credentials Error.");
    when_failed_trace(ftx_request_set_target_file(request, new_file_name), fail, ERROR, "There is no such file.");
    when_failed_trace(ftx_request_set_max_transfer_time(request, 600), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_data(request, strdup(new_file_name)), fail, ERROR, "Could not set userdata.");
    when_failed_trace(ftx_request_set_upload_method(request, ftx_upload_method_post), fail, ERROR, "Could not set HTTP method.");
    when_failed_trace(ftx_request_set_http_multipart_form_data(request, "file", file_name, "application/octet-stream"), fail, ERROR, "Could not set HTTP multipart.");

    if(col->file_ahdcp_url && (strstr(col->file_ahdcp_url, "https://") != NULL)) {
        int rc = -1;
        char ca_cert_path[254];
        if(col->file_ahdcp_crt_name) {
            when_str_empty_trace(col->file_ahdcp_crt_name, fail, ERROR, "CA Certificate is not defined");
            sprintf(ca_cert_path, "%s/%s/%s/%s", LCM_CONTAINER_PATH, col->uuid, CRT_PATH, col->file_ahdcp_crt_name);
            rc = ftx_request_set_ca_certificates(request, ca_cert_path, NULL);
            when_failed_trace(rc, fail, ERROR, "Could not set CA certificate");
            rc = ftx_request_set_client_certificates(request, NULL, "/etc/config/autocert/cpe_client.crt.pem", "/etc/config/autocert/cpe_client.key.pem");
            when_failed_trace(rc, fail, ERROR, "Could not set client crt and key");
        }
    }

    when_failed(ftx_request_send(request), fail);

fail:
    return;
}

static void agent_file_wait_timer_cb(amxp_timer_t* timer, void* priv) {
    SAH_TRACEZ_INFO(ME, "Wait time is over");
    amxp_timer_delete(&timer); // delete timer
    col_conf_t* col = (col_conf_t*) priv;
    agent_dm_update_status(col->ahdcpfile_obj, "Enabled");
    col->file_wait_timer = NULL;
}

static void agent_file_write(col_conf_t* col, const char* msg, uint32_t msg_len) {
    if(col->file_wait_timer) {
        SAH_TRACEZ_INFO(ME, "Col %s: Can't write to file. File wait timer still on", col->name);
        return;
    }
    uint32_t file_max_allowed_size = agent_dm_get_ahdcpfile_config_param(col, "MaxFileSize");
    uint32_t file_wait_interval = agent_dm_get_ahdcpfile_config_param(col, "WaitInterval");

    if(col->file_cur_size + msg_len >= file_max_allowed_size) {
        SAH_TRACEZ_INFO(ME, "Col %s: File max size reached", col->name);
        // if max volume reached start wait timer
        amxp_timer_new(&col->file_wait_timer, agent_file_wait_timer_cb, col);
        amxp_timer_start(col->file_wait_timer, file_wait_interval * 1000);

        // send file
        agent_file_transfer(col);
        agent_dm_update_status(col->ahdcpfile_obj, "Stopped");
    } else {
        SAH_TRACEZ_INFO(ME, "Col %s: write message to file", col->name);
        char file_path[232];
        sprintf(file_path, "%s/%s/%s/%s", LCM_CONTAINER_PATH, col->uuid, BASE_PATH, FILE_NAME);
        FILE* fp = fopen(file_path, "a");
        if(fp) {
            SAH_TRACEZ_INFO(ME, "File opened");
            if(fprintf(fp, "%s\n", msg) > 0) {
                fflush(fp);
                col->file_cur_size += (msg_len + 1);
                SAH_TRACEZ_INFO(ME, "Wrote message to file");
            }
            fclose(fp);
        }
    }
}

void update_dm_file(col_conf_t* col) {
    SAH_TRACEZ_INFO(ME, "Col %s: Update data model Stats of file-based AhDCP", col->name);
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("DataCollect.");
    char path[256];
    amxc_var_t ret;
    amxc_var_t req_paths;
    amxc_var_t* req_path = NULL;
    amxc_var_t* params = NULL;

    amxc_var_init(&req_paths);
    amxc_var_set_type(&req_paths, AMXC_VAR_ID_LIST);
    amxc_var_init(&ret);

    sprintf(path, "DataCollect.AhDSource.%d.AhDCPfile.Stats.", col->pos);

    req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
    amxc_var_add_key(cstring_t, req_path, "path", path);
    params = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
    amxc_var_add_key(uint32_t, params, "CurrentFileSize", col->file_cur_size);
    amxc_var_add_key(uint32_t, params, "NumberFilesSent", col->file_num_sent);

    amxb_set_multiple(bus_ctx, 0, &req_paths, &ret, 5);
    amxc_var_clean(&ret);
    amxc_var_clean(&req_paths);
}

void file_based_update(col_conf_t* col, const char* msg, uint32_t msg_len) {
    agent_file_write(col, msg, msg_len);
    update_dm_file(col);
}

bool agent_get_ahdcpfile_enables(col_conf_t* col) {
    bool agent_enable = amxd_object_get_value(bool, col->agent_obj, "Enable", NULL);
    bool ahdsource_enable = amxd_object_get_value(bool, col->ahdsource_obj, "Enable", NULL);
    bool ahdcpfile_enable = amxd_object_get_value(bool, col->ahdcpfile_obj, "Enable", NULL);

    return agent_enable & ahdsource_enable & ahdcpfile_enable;
}
