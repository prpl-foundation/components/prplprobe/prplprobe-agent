/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _DEFAULT_SOURCE

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <debug/sahtrace.h>

#include "agent.h"
#include "dm_agent.h"
#include "agent_deviceinfo.h"
#include "agent_tr181-mqtt.h"
#include "agent_collector.h"
#include "agent_file.h"

#define ME "dm_methods"

static int random_num = 0;

/* Collector Point type*/
//copied from datacollect.h
typedef enum cp_type {
    MESSAGE_T = 0,
    FILE_T,
    ALL_T
} cp_type_t;

static void toUpper(char* str) {
    for(size_t i = 0; i < strlen(str); i++) {
        char c = str[i];
        str[i] = toupper(c);
    }
}

amxd_status_t _logEvent(amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "logEvent called");
    amxd_status_t retval = amxd_status_unknown_error;
    amxc_var_init(ret);
    const char* message = NULL;
    char topic[512];
    char dataid[256];

    when_null(object, exit);

    const char* uuid = GET_CHAR(args, "source");
    message = GET_CHAR(args, "message");
    uint32_t msg_len = GET_UINT32(args, "message_len");
    const char* data_id = GET_CHAR(args, "data_id");
    uint8_t cp_type = GET_UINT32(args, "cp_type");

    SAH_TRACEZ_INFO(ME, "Source: %s", uuid);
    SAH_TRACEZ_INFO(ME, "New messsage: %s", message);
    SAH_TRACEZ_INFO(ME, "message_len: %d", msg_len);
    SAH_TRACEZ_INFO(ME, "CP type: %d", cp_type);


    col_conf_t* col;
    uint32_t ts = 0;
    amxc_llist_t* col_lst = get_col_lst();
    amxc_llist_for_each(it, col_lst) {
        col = amxc_container_of(it, col_conf_t, it);
        if(strcmp(uuid, col->uuid) == 0) {
            SAH_TRACEZ_INFO(ME, "Collector found on list");
            if((cp_type == ALL_T) || (cp_type == MESSAGE_T)) {
                if(agent_get_ahdcpmessage_enables(col)) {
                    ts = msg_based_update(col, msg_len);
                    // ts = 0 means the collector has not passed its max volumetry
                    if(ts) {
                        SAH_TRACEZ_INFO(ME, "Collector stopped, will not send/write message");
                    } else {
                        const char* cp_name = "cpe-mqtt-fut";          //TODO: for now use only the broker from the FUT
                        char group[32] = "figaro";                     // TODO: read group from the config installed with the LCM container
                        char base_topic[64] = "sah/to/service/eyeson"; // TODO: read this from the config
                        strcpy(dataid, data_id);
                        toUpper(dataid);
                        sprintf(topic, "%s/%s/%s/%d/event/%s", base_topic, group, get_deviceinfo_serialnumber(), random_num, dataid);
                        SAH_TRACEZ_INFO(ME, "Topic: %s", topic);

                        tr181_mqtt_publish(cp_name, topic, message);
                    }
                } else {
                    SAH_TRACEZ_INFO(ME, "Not sending message. Disabled mode active");
                }
            }
            if((cp_type == ALL_T) || (cp_type == FILE_T)) {
                if(agent_get_ahdcpfile_enables(col)) {
                    file_based_update(col, message, msg_len);
                } else {
                    SAH_TRACEZ_INFO(ME, "Not writing message. Disabled mode active");
                }
            }
        }
    }
    amxc_var_set(uint32_t, ret, ts);

    retval = amxd_status_ok;
exit:
    return retval;
}


amxd_status_t _registerAhDSource(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    const char* collector_uuid = NULL;
    //const char *conf_file_name = NULL;

    when_null(object, exit);

    collector_uuid = GET_CHAR(args, "source");
    //conf_file_name = GET_CHAR(args, "conf_file_name");

    SAH_TRACEZ_INFO(ME, "New AhDSource registered: %s", collector_uuid);
    //SAH_TRACEZ_INFO(ME, "Configuration file name: %s", conf_file_name);

    retval = amxd_status_ok;
    random_num = (int) rand() % 100; // TODO:

    register_col(collector_uuid);

exit:
    return retval;
}
