/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "agent_ts_sync.h"

#define ME "ts_sync"

// timers aligned with the natural calendar/clock boundaries
static amxp_timer_t* day_timer = NULL; // triggers at midnight
static amxp_timer_t* hr_timer = NULL;
static amxp_timer_t* min_timer = NULL;
static amxp_timer_t* sec_timer = NULL;

static void agent_first_midnight_sync(amxp_timer_t* timer, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Sync day timeslot");
    amxp_timer_delete(&timer); // delete initial timer

    /* Initialize timer */
    amxp_timer_new(&day_timer, col_update_day_stats, NULL);
    amxp_timer_set_interval(day_timer, 86400 * 1000);
    amxp_timer_start(day_timer, 0);
}

static void agent_first_hour_sync(amxp_timer_t* timer, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Sync hr timeslot");
    amxp_timer_delete(&timer); // delete initial timer

    /* Initialize timer */
    amxp_timer_new(&hr_timer, col_update_hr_stats, NULL);
    amxp_timer_set_interval(hr_timer, 3600 * 1000);
    amxp_timer_start(hr_timer, 0);
}

static void agent_first_min_sync(amxp_timer_t* timer, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Sync min timeslot");
    amxp_timer_delete(&timer); // delete initial timer

    /* Initialize timer */
    amxp_timer_new(&min_timer, col_update_min_stats, NULL);
    amxp_timer_set_interval(min_timer, 60 * 1000);
    amxp_timer_start(min_timer, 0);
}

static void agent_first_sec_sync(amxp_timer_t* timer, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Sync sec timeslot");
    amxp_timer_delete(&timer); // delete initial timer

    /* Initialize timer */
    amxp_timer_new(&sec_timer, col_update_sec_stats, NULL);
    amxp_timer_set_interval(sec_timer, 1 * 1000);
    amxp_timer_start(sec_timer, 0);
}

// sync the timeslots with the natural clock boundaries
void init_sync_timers(void) {
    char time_buffer[256];
    struct timeval now;
    gettimeofday(&now, NULL);
    strftime(time_buffer, sizeof(time_buffer), "%FT%TZ", gmtime((time_t*) &now.tv_sec));
    SAH_TRACEZ_INFO(ME, "Current time [%lu]: %s", (unsigned long) now.tv_sec, time_buffer);
    uint32_t sec_until_midnight = 86400 - (now.tv_sec % 86400);
    SAH_TRACEZ_INFO(ME, "First start. %d sec until midnight", sec_until_midnight);

    /* Initialize timer */
    amxp_timer_new(&day_timer, agent_first_midnight_sync, NULL);
    amxp_timer_start(day_timer, sec_until_midnight * 1000);

    uint32_t sec_until_start_of_next_hr = 3600 - (now.tv_sec % 3600);
    SAH_TRACEZ_INFO(ME, "First start. %d sec until next hour", sec_until_start_of_next_hr);
    amxp_timer_t* hour_timer = NULL;
    /* Initialize timer */
    amxp_timer_new(&hour_timer, agent_first_hour_sync, NULL);
    amxp_timer_start(hour_timer, sec_until_start_of_next_hr * 1000);

    uint32_t sec_until_start_of_next_min = 60 - (now.tv_sec % 60);
    SAH_TRACEZ_INFO(ME, "First start. %d sec until next min", sec_until_start_of_next_min);
    amxp_timer_t* min_sync_timer = NULL;
    /* Initialize timer */
    amxp_timer_new(&min_sync_timer, agent_first_min_sync, NULL);
    amxp_timer_start(min_sync_timer, sec_until_start_of_next_min * 1000);

    uint32_t usec_until_start_of_next_sec = 1000000 - now.tv_usec;
    SAH_TRACEZ_INFO(ME, "First start. %d ms until next sec", usec_until_start_of_next_sec / 1000);
    amxp_timer_t* sec_sync_timer = NULL;
    /* Initialize timer */
    amxp_timer_new(&sec_sync_timer, agent_first_sec_sync, NULL);
    amxp_timer_start(sec_sync_timer, usec_until_start_of_next_sec / 10000);
}

void cleanup_sync_timers(void) {
    amxp_timer_delete(&day_timer);
    amxp_timer_delete(&hr_timer);
    amxp_timer_delete(&min_timer);
    amxp_timer_delete(&sec_timer);
}
