/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "agent.h"
#include "agent_deviceinfo.h"
#include "agent_collector.h"
#include "agent_ts_sync.h"

#define ME "agent"

static agent_app_t agent;

amxd_dm_t* agent_get_dm(void) {
    return agent.dm;
}

amxo_parser_t* agent_get_parser(void) {
    return agent.parser;
}

amxd_object_t* agent_get_root_object(void) {
    const char* prefix = amxc_var_constcast(cstring_t, amxo_parser_get_config(agent.parser, "prefix_"));
    amxc_string_t object_name;
    amxd_object_t* root_object = NULL;

    amxc_string_init(&object_name, 0);
    amxc_string_setf(&object_name, "%s", ROOT_OBJECT_NAME);

    if((prefix != NULL) && (*prefix != 0)) {
        amxc_string_prependf(&object_name, "%s", prefix);
    }
    root_object = amxd_dm_findf(agent_get_dm(), "%s", amxc_string_get(&object_name, 0));
    amxc_string_clean(&object_name);
    return root_object;
}

amxd_object_t* agent_get_object(const char* path);
amxd_object_t* agent_get_object(const char* path) {
    amxd_object_t* object = NULL;

    when_str_empty(path, exit);
    object = amxd_dm_findf(agent_get_dm(), "%s", path);

exit:
    return object;
}


bool agent_get_enable(void) {
    return amxd_object_get_value(bool, agent_get_root_object(), "Enable", NULL);
}

static void agent_synchronized_done(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    const char* time_obj = "Time.";
    amxb_bus_ctx_t* ctx = amxb_be_who_has(time_obj);

    SAH_TRACEZ_INFO(ME, "Time sync done");
    agent.synchronized = true;
    amxb_unsubscribe(ctx, time_obj, agent_synchronized_done, NULL);

    if(agent_get_enable()) {
        //do somethig here
    }
}

static void agent_setup_time_sync(void) {
    const char* status = NULL;
    const char* time_obj = "Time.";
    amxb_bus_ctx_t* ctx = amxb_be_who_has(time_obj);
    amxc_var_t ret;

    amxc_var_init(&ret);

    // It is possible that Status is already Synchronized, so use subscribe + get
    // Do subscribe before get, because otherwise you can theoretically miss the Status change
    amxb_subscribe(ctx, time_obj, "parameters.Status.to == 'Synchronized'",
                   agent_synchronized_done, NULL);
    amxb_get(ctx, time_obj, 0, &ret, 5);

    status = GETP_CHAR(&ret, "0.0.Status");
    if((status != NULL) && (strcmp(status, "Synchronized") == 0)) {
        agent.synchronized = true;
        amxb_unsubscribe(ctx, time_obj, agent_synchronized_done, NULL);
    }

    amxc_var_clean(&ret);
}

static void filetransfer_event_cb(int fd, UNUSED void* priv) {
    ftx_fd_event_handler(fd);
}

static int filetransfer_fdset_cb(int fd, bool add) {
    int retval = -1;
    if(add) {
        retval = amxo_connection_add(agent_get_parser(), fd, filetransfer_event_cb, NULL, AMXO_LISTEN, NULL);
    } else {
        retval = amxo_connection_remove(agent_get_parser(), fd);
    }
    SAH_TRACEZ_INFO(ME, "%s connection fd %d %s", add ? "add" : "remove", fd, (retval == 0) ? "success" : "failure");
    return retval;
}

static int agent_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    bool needs_time_sync = false;

    SAH_TRACEZ_INFO(ME, "agent start");
    agent.dm = dm;
    agent.parser = parser;

    if(parser != NULL) {
        needs_time_sync = GET_BOOL(&parser->config, "needs-time-sync");
    }
    if(needs_time_sync) {
        SAH_TRACEZ_INFO(ME, "Time sync needed");
        agent_setup_time_sync();
    } else {
        SAH_TRACEZ_INFO(ME, "Time sync not needed");
        agent.synchronized = true;
    }

    dc_agent_deviceinfo_init();
    init_col_lst();
    init_sync_timers();
    ftx_init(filetransfer_fdset_cb);

    return 0;
}

static int agent_clean(void) {
    SAH_TRACEZ_INFO(ME, "agent stop");

    agent.dm = NULL;
    agent.parser = NULL;
    if(!agent.synchronized) {
        const char* time_obj = "Time.";
        amxb_bus_ctx_t* ctx = amxb_be_who_has(time_obj);
        amxb_unsubscribe(ctx, time_obj, agent_synchronized_done, NULL);
    }
    agent.synchronized = false;

    dc_agent_deviceinfo_cleanup();
    cleanup_sync_timers();
    ftx_clean();

    return 0;
}

static amxd_trans_t* agent_init_transaction(const char* obj_path) {
    amxd_trans_t* transaction;
    amxd_dm_t* dm = agent_get_dm();
    amxd_object_t* agent_obj = amxd_dm_findf(dm, "%s", obj_path);
    amxd_trans_new(&transaction);
    amxd_trans_set_attr(transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(transaction, agent_obj);
    return transaction;
}

void agent_set_status(agent_status_e status) {
    const char* status_string = NULL;
    amxd_object_t* root = agent_get_root_object();
    const char* object_name = amxd_object_get_name(root, AMXD_OBJECT_NAMED);
    amxd_trans_t* transaction = NULL;

    when_null(root, exit);
    when_str_empty(object_name, exit);

    switch(status) {
    case AGENT_DISABLED:
        status_string = "Disabled";
        break;
    case AGENT_ENABLED:
        status_string = "Enabled";
        break;
    case AGENT_ERROR:
        status_string = "Error";
        break;
    }

    transaction = agent_init_transaction(object_name);
    amxd_trans_set_value(cstring_t, transaction, "Status", status_string);
    amxd_trans_apply(transaction, agent_get_dm());

exit:
    amxd_trans_delete(&transaction);
}

// Used to update Status of AhDSource or a specific flow
void agent_dm_update_status(amxd_object_t* obj, const char* status) {
    amxd_dm_t* dm = agent_get_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);
}

//void agent_set_dm_param_uint32(const char* path, const char* status) {
//    amxd_dm_t* dm = agent_get_dm();
//    if(dm == NULL) {
//        SAH_TRACEZ_INFO(ME, "dm is NULL");
//    }
//    amxd_trans_t trans;
//    amxd_object_t* object = NULL;
//    object = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
//    if (object == NULL) {
//        SAH_TRACEZ_INFO(ME, "Object is NULL");
//    }
//
//    amxd_trans_init(&trans);
//    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
//    amxd_trans_select_object(&trans, obj);
//    amxd_trans_set_value(cstring_t, &trans, "Status", status);
//    amxd_trans_apply(&trans, dm);
//
//    amxd_trans_clean(&trans);
//}

int _agent_main(int reason,
                amxd_dm_t* dm,
                amxo_parser_t* parser) {
    int rv = -1;
    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);

    switch(reason) {
    case AMXO_START:     // START
        rv = agent_init(dm, parser);
        break;
    case AMXO_STOP:     // STOP
        rv = agent_clean();
        break;
    }

    return rv;
}

