/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "agent_collector.h"
#include "agent_file.h"

#define CONF_FILE_NAME "datacollect-config.cfg"
#define FILE_PATH "/etc/config/datacollect/"
//#define FILE_PATH "/home/sahakka10/workspaces/eyeson_prpl/"
//#define LCM_CONTAINER_PATH "/"

#define ME "collector"


// list of registered collectors
static amxc_llist_t col_lst;

void update_dm_ongoing_ts(col_conf_t* col);

amxc_llist_t* get_col_lst(void) {
    return &col_lst;
}

// parse json file with the config of the collector
static int parse_col_config_file(const char* file, col_conf_t* new_col) {
    amxc_var_t* var = NULL;
    const amxc_var_t* part = NULL;
    variant_json_t* reader = NULL;
    int read_length = 0;
    int fd = open(file, O_RDONLY);
    SAH_TRACEZ_INFO(ME, "Reading Collector config");

    if(fd < 0) {
        SAH_TRACEZ_ERROR(ME, "Error opening collector config file [%s]", file);
        return 1;
    }

    if(amxj_reader_new(&reader) != 0) {
        SAH_TRACEZ_ERROR(ME, "Error creating new amxj reader");
        return 1;
    }
    if(reader == NULL) {
        SAH_TRACEZ_ERROR(ME, "Error amxj reader is NULL");
        return 1;
    }

    read_length = amxj_read(reader, fd);
    while(read_length > 0) {
        read_length = amxj_read(reader, fd);
    }

    var = amxj_reader_result(reader);
    if(var == NULL) {
        SAH_TRACEZ_ERROR(ME, "Error amxj reader result is NULL");
        return 1;
    }

    if(amxc_var_type_of(var) != AMXC_VAR_ID_HTABLE) {
        SAH_TRACEZ_ERROR(ME, "Error: config file not with appropriate format");
        return 1;
    }

    // Start reading the json config file contents
    part = amxc_var_get_key(var, "name", AMXC_VAR_FLAG_DEFAULT);
    if(amxc_var_type_of(part) != AMXC_VAR_ID_CSTRING) {
        SAH_TRACEZ_ERROR(ME, "Error: field 'name' is not a string");
        return 1;
    }
    const char* name = amxc_var_constcast(cstring_t, part);
    SAH_TRACEZ_INFO(ME, "Collector name: %s", name);
    if(name && strcmp(name, "")) {
        new_col->name = strdup(name);
    } else {
        new_col->name = strdup("not-defined");
    }

    part = amxc_var_get_key(var, "ahdcp", AMXC_VAR_FLAG_DEFAULT);
    if(amxc_var_type_of(part) != AMXC_VAR_ID_LIST) {
        SAH_TRACEZ_ERROR(ME, "Error: field 'ahdcp' is not a list");
        SAH_TRACEZ_ERROR(ME, "Error: missing essential information, new collector not started");
        return 1;
    }

    // Read ahdcp
    const amxc_var_t* part2 = NULL;
    const amxc_var_t* config = NULL;
    const amxc_var_t* part3 = NULL;
    const char* ahdcp_name = NULL;
    bool auto_restart;
    uint32_t num;
    int n_cp = 0;
    amxc_llist_for_each(it, amxc_var_constcast(amxc_llist_t, part)) {
        amxc_var_t* item = amxc_var_from_llist_it(it);
        n_cp++;

        if(amxc_var_type_of(item) != AMXC_VAR_ID_HTABLE) {
            SAH_TRACEZ_ERROR(ME, "Error: config file not with appropriate format");
            return 1;
        }

        part2 = amxc_var_get_key(item, "name", AMXC_VAR_FLAG_DEFAULT);
        ahdcp_name = amxc_var_constcast(cstring_t, part2);
        SAH_TRACEZ_INFO(ME, "name: %s", ahdcp_name);
        part2 = amxc_var_get_key(item, "type", AMXC_VAR_FLAG_DEFAULT);
        const char* type = amxc_var_constcast(cstring_t, part2);
        SAH_TRACEZ_INFO(ME, "type: %s", type);

        config = amxc_var_get_key(item, "config", AMXC_VAR_FLAG_DEFAULT);
        if(amxc_var_type_of(config) != AMXC_VAR_ID_HTABLE) {
            SAH_TRACEZ_ERROR(ME, "Error: field 'config' is not a hash table");
            return 1;
        }
        part3 = amxc_var_get_key(config, "AutomaticRestartEnable", AMXC_VAR_FLAG_DEFAULT);
        auto_restart = amxc_var_constcast(bool, part3);
        SAH_TRACEZ_INFO(ME, "AutomaticRestartEnable: %d", auto_restart);

        if(strcmp(type, "message-based") == 0) {
            new_col->msg_ahdcp_name = strdup(ahdcp_name);
            new_col->msg_auto_restart = auto_restart;
            part3 = amxc_var_get_key(config, "MaxAllowedBytesPerSecond", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                SAH_TRACEZ_ERROR(ME, "Error: field 'MaxAllowedBytesPerSecond' is not an integer");
                return 1;
            }
            num = amxc_var_constcast(int64_t, part3);
            SAH_TRACEZ_INFO(ME, "MaxAllowedBytesPerSecond: %d", num);
            new_col->max_bytes_per_sec = num;

            part3 = amxc_var_get_key(config, "MaxAllowedBytesPerMinute", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                SAH_TRACEZ_ERROR(ME, "Error: field 'MaxAllowedBytesPerMinute' is not an integer");
                return 1;
            }
            num = amxc_var_constcast(int64_t, part3);
            SAH_TRACEZ_INFO(ME, "MaxAllowedBytesPerMinute: %d", num);
            new_col->max_bytes_per_min = num;

            part3 = amxc_var_get_key(config, "MaxAllowedBytesPerHour", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                SAH_TRACEZ_ERROR(ME, "Error: field 'MaxAllowedBytesPerHour' is not an integer");
                return 1;
            }
            num = amxc_var_constcast(int64_t, part3);
            SAH_TRACEZ_INFO(ME, "MaxAllowedBytesPerHour: %d", num);
            new_col->max_bytes_per_hr = num;

            part3 = amxc_var_get_key(config, "MaxAllowedBytesPerDay", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                SAH_TRACEZ_ERROR(ME, "Error: field 'MaxAllowedBytesPerDay' is not an integer");
                return 1;
            }
            num = amxc_var_constcast(int64_t, part3);
            SAH_TRACEZ_INFO(ME, "MaxAllowedBytesPerDay: %d", num);
            new_col->max_bytes_per_day = num;
        } else if(strcmp(type, "file-based") == 0) {
            new_col->file_ahdcp_name = strdup(ahdcp_name);
            new_col->file_auto_restart = auto_restart;

            part2 = amxc_var_get_key(item, "url", AMXC_VAR_FLAG_DEFAULT);
            const char* url = amxc_var_constcast(cstring_t, part2);
            if(url) {
                new_col->file_ahdcp_url = strdup(url);
                SAH_TRACEZ_INFO(ME, "AhDCP URL: %s", url);
            }

            part2 = amxc_var_get_key(item, "ca_certificate", AMXC_VAR_FLAG_DEFAULT);
            const char* crt = amxc_var_constcast(cstring_t, part2);
            if(crt) {
                new_col->file_ahdcp_crt_name = strdup(crt);
                SAH_TRACEZ_INFO(ME, "AhDCP CA certificate name: %s", crt);
            }

            // MaxAllowedSize
            part3 = amxc_var_get_key(config, "MaxAllowedSize", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) == AMXC_VAR_ID_INVALID) {
                SAH_TRACEZ_ERROR(ME, "Field 'MaxAllowedSize' not set on config");
            } else {
                if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                    SAH_TRACEZ_ERROR(ME, "Error: field 'MaxAllowedSize' is not an integer");
                } else {
                    num = amxc_var_constcast(int64_t, part3);
                    SAH_TRACEZ_INFO(ME, "MaxAllowedSize: %d", num);
                    new_col->file_max_allowed_size = num;
                }
            }

            // SendInterval
            part3 = amxc_var_get_key(config, "SendInterval", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) == AMXC_VAR_ID_INVALID) {
                SAH_TRACEZ_ERROR(ME, "Field 'SendInterval' not set on config");
            } else {
                if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                    SAH_TRACEZ_ERROR(ME, "Error: field 'SendInterval' is not an integer");
                } else {
                    num = amxc_var_constcast(int64_t, part3);
                    SAH_TRACEZ_INFO(ME, "SendInterval: %d", num);
                    new_col->file_send_interval = num;
                }
            }

            // WaitInterval
            part3 = amxc_var_get_key(config, "WaitInterval", AMXC_VAR_FLAG_DEFAULT);
            if(amxc_var_type_of(part3) == AMXC_VAR_ID_INVALID) {
                SAH_TRACEZ_ERROR(ME, "Field 'WaitInterval' not set on config");
            } else {
                if(amxc_var_type_of(part3) != AMXC_VAR_ID_INT64) {
                    SAH_TRACEZ_ERROR(ME, "Error: field 'WaitInterval' is not an integer");
                } else {
                    num = amxc_var_constcast(int64_t, part3);
                    SAH_TRACEZ_INFO(ME, "WaitInterval: %d", num);
                    new_col->file_wait_interval = num;
                }
            }
        }
    }

    close(fd);
    amxc_var_delete(&var);
    if(!n_cp) {
        SAH_TRACEZ_ERROR(ME, "Error: Collector with 0 AhDCP configured");
        return 1;
    }

    return 0;
}


// adapted from amx/qos/applications/tr181-qos/src/dm-qos-node.c
// add data model instance of collector under AhDSource table
static void add_col_to_dm(col_conf_t* new_col) {
    amxd_object_t* ahds_obj = NULL;
    ahds_obj = amxd_dm_findf(agent_get_dm(), "%s", "DataCollect.AhDSource");
    if(!ahds_obj) {
        SAH_TRACEZ_ERROR(ME, "AhDSource object not found");
    }

    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Name", new_col->name);
    amxc_var_add_key(cstring_t, &params, "Alias", new_col->name);

    amxc_var_t args, ret;
    amxd_status_t status;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(amxc_htable_t, &args, "parameters", amxc_var_constcast(amxc_htable_t, &params));
    amxc_var_add_key(uint32_t, &args, "access", amxd_dm_access_public);

    status = amxd_action_object_add_inst(ahds_obj, NULL, action_object_add_inst, &args, &ret, NULL);
    if((status == amxd_status_ok) || (status == amxd_status_duplicate)) {
        SAH_TRACEZ_ERROR(ME, "New AhDSource added to data model");
        amxd_dm_t* dm = agent_get_dm();
        char path[256];
        //keep a reference to data model objects that have config parameters
        sprintf(path, "DataCollect.");
        new_col->agent_obj = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
        sprintf(path, "DataCollect.AhDSource.%d.", new_col->pos);
        new_col->ahdsource_obj = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
        sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.", new_col->pos);
        new_col->ahdcpmessage_obj = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
        sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.Config.", new_col->pos);
        new_col->ahdcpmessage_config_obj = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
        sprintf(path, "DataCollect.AhDSource.%d.AhDCPfile.", new_col->pos);
        new_col->ahdcpfile_obj = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
        sprintf(path, "DataCollect.AhDSource.%d.AhDCPfile.Config.", new_col->pos);
        new_col->ahdcpfile_config_obj = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&params);
    amxc_var_clean(&ret);

    amxc_var_t req_paths;
    amxc_var_t* req_path = NULL;
    amxc_var_t* params2 = NULL;

    amxc_var_init(&req_paths);
    amxc_var_set_type(&req_paths, AMXC_VAR_ID_LIST);
    amxc_var_init(&ret);
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("DataCollect.");
    char* path = NULL;
    if(new_col->msg_ahdcp_name) {
        if(asprintf(&path, "DataCollect.AhDSource.%d.AhDCPmessage.", new_col->pos) < 0) {
            SAH_TRACEZ_ERROR(ME, "No memory available");
            return;
        }

        req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
        amxc_var_add_key(cstring_t, req_path, "path", path);
        params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
        amxc_var_add_key(cstring_t, params2, "Name", new_col->msg_ahdcp_name);

        free(path);
        path = NULL;

        // set Config object
        if(asprintf(&path, "DataCollect.AhDSource.%d.AhDCPmessage.Config.", new_col->pos) < 0) {
            SAH_TRACEZ_ERROR(ME, "No memory available");
            return;
        }

        req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
        amxc_var_add_key(cstring_t, req_path, "path", path);
        params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
        amxc_var_add_key(bool, params2, "AutomaticRestartEnable", new_col->msg_auto_restart);
        amxc_var_add_key(int64_t, params2, "MaxAllowedBytesPerDay", new_col->max_bytes_per_day);
        amxc_var_add_key(int64_t, params2, "MaxAllowedBytesPerHour", new_col->max_bytes_per_hr);
        amxc_var_add_key(int64_t, params2, "MaxAllowedBytesPerMinute", new_col->max_bytes_per_min);
        amxc_var_add_key(int64_t, params2, "MaxAllowedBytesPerSecond", new_col->max_bytes_per_sec);
        free(path);
        path = NULL;
    }

    if(new_col->file_ahdcp_name) {
        if(asprintf(&path, "DataCollect.AhDSource.%d.AhDCPfile.", new_col->pos) < 0) {
            SAH_TRACEZ_ERROR(ME, "No memory available");
            return;
        }

        req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
        amxc_var_add_key(cstring_t, req_path, "path", path);
        params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
        amxc_var_add_key(cstring_t, params2, "Name", new_col->file_ahdcp_name);

        free(path);
        path = NULL;

        // set Config object
        if(asprintf(&path, "DataCollect.AhDSource.%d.AhDCPfile.Config.", new_col->pos) < 0) {
            SAH_TRACEZ_ERROR(ME, "No memory available");
            return;
        }

        req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
        amxc_var_add_key(cstring_t, req_path, "path", path);
        params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
        amxc_var_add_key(bool, params2, "AutomaticRestartEnable", new_col->file_auto_restart);
        amxc_var_add_key(int64_t, params2, "MaxFileSize", new_col->file_max_allowed_size);
        amxc_var_add_key(int64_t, params2, "SendInterval", new_col->file_send_interval);
        amxc_var_add_key(int64_t, params2, "WaitInterval", new_col->file_wait_interval);

        free(path);
        path = NULL;
    }

    amxb_set_multiple(bus_ctx, 0, &req_paths, &ret, 5);
    amxc_var_clean(&ret);
    amxc_var_clean(&req_paths);
}

static void del_col(col_conf_t* col) {
    if(!col) {
        SAH_TRACEZ_ERROR(ME, "Collector is NULL");
        return;
    }

    if(col->name) {
        free(col->name);
    }
    if(col->uuid) {
        free(col->uuid);
    }
    if(col->msg_ahdcp_name) {
        free(col->msg_ahdcp_name);
    }
    if(col->file_ahdcp_name) {
        free(col->file_ahdcp_name);
    }
    if(col->file_ahdcp_url) {
        free(col->file_ahdcp_url);
    }
    if(col->file_ahdcp_crt_name) {
        free(col->file_ahdcp_crt_name);
    }
    if(col->file_send_timer) {
        amxp_timer_delete(&col->file_send_timer);
    }
    if(col->file_wait_timer) {
        amxp_timer_delete(&col->file_wait_timer);
    }

    // TODO: delete stats?

    free(col);
    col = NULL;
}

static void agent_reset_stats_analysis(col_conf_t* col) {
    col->min_bytes_sent_perhr = UINT32_MAX;
    col->min_bytes_sent_permin = UINT32_MAX;
    col->min_bytes_sent_persec = UINT32_MAX;
    col->max_bytes_sent_perhr = 0;
    col->max_bytes_sent_permin = 0;
    col->max_bytes_sent_persec = 0;
    col->min_msg_sent_perhr = UINT32_MAX;
    col->min_msg_sent_permin = UINT32_MAX;
    col->min_msg_sent_persec = UINT32_MAX;
    col->max_msg_sent_perhr = 0;
    col->max_msg_sent_permin = 0;
    col->max_msg_sent_persec = 0;
    col->num_sec_ts = 1;
    col->num_min_ts = 1;
    col->num_hr_ts = 1;
}

static void agent_calculate_avg(col_conf_t* col) {
    col->avg_bytes_sec_ts = col->cur_bytes_per_day / col->num_sec_ts;
    col->avg_bytes_min_ts = col->cur_bytes_per_day / col->num_min_ts;
    col->avg_bytes_hr_ts = col->cur_bytes_per_day / col->num_hr_ts;

    col->avg_msg_sec_ts = (double) col->cur_msg_sent_per_day / (double) col->num_sec_ts;
    col->avg_msg_min_ts = col->cur_msg_sent_per_day / col->num_min_ts;
    col->avg_msg_hr_ts = col->cur_msg_sent_per_day / col->num_hr_ts;
}

static void init_col(col_conf_t** col, const char* uuid) {
    *col = (col_conf_t*) calloc(1, sizeof(col_conf_t));
    //set default values
    (*col)->uuid = strdup(uuid);
    (*col)->max_bytes_per_sec = 1000;
    (*col)->max_bytes_per_min = 10000;
    (*col)->max_bytes_per_hr = 100000;
    (*col)->max_bytes_per_day = 1000000;
    (*col)->max_bytes_per_day = 1000000;
    (*col)->file_max_allowed_size = 10000000;
    (*col)->file_send_interval = 3600;
    (*col)->file_max_allowed_size = 1000;
    agent_reset_stats_analysis(*col);
}

static int read_col_config_file(const char* uuid, col_conf_t* new_col) {
    char file[256];
    int rv = 0;

    sprintf(file, "%s/%s/%s/%s", LCM_CONTAINER_PATH, uuid, FILE_PATH, CONF_FILE_NAME);
    if(parse_col_config_file(file, new_col)) {
        SAH_TRACEZ_ERROR(ME, "Failed to read collector config file");
        rv = 1;
    }
    return rv;
}

static void send_file_cb(amxp_timer_t* timer UNUSED, void* priv) {
    col_conf_t* col = (col_conf_t*) priv;
    SAH_TRACEZ_INFO(ME, "Col %s: Send File Interval reached", col->name);

    // send file
    agent_file_transfer(col);
    update_dm_file(col);
}

// parse json file, add collector to list and add collector to data model
void register_col(const char* uuid) {
    col_conf_t* new_col = NULL;
    init_col(&new_col, uuid);
    if(!read_col_config_file(uuid, new_col)) {
        /* Initialize File send timer */
        amxp_timer_new(&new_col->file_send_timer, send_file_cb, new_col);
        amxp_timer_set_interval(new_col->file_send_timer, new_col->file_send_interval * 1000);
        amxp_timer_start(new_col->file_send_timer, new_col->file_send_interval * 1000);

        SAH_TRACEZ_INFO(ME, "Add new collector to list");
        amxc_llist_append(&col_lst, &new_col->it);
        new_col->pos = amxc_llist_size(&col_lst);
        add_col_to_dm(new_col);
    } else {
        SAH_TRACEZ_ERROR(ME, "Parsing of collector config failed");
        del_col(new_col);
    }
}

void init_col_lst(void) {
    amxc_llist_init(&col_lst);
}

//static void agent_dm_set_obj_param(const char* path, const char* param_name, uint32_t param_value) {
////    amxd_dm_t* dm = agent_get_dm();
////    amxd_trans_t trans;
////
////    amxd_trans_init(&trans);
////    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
////    amxd_trans_select_object(&trans, obj);
////    amxd_trans_set_value(cstring_t, &trans, "Status", status);
////    amxd_trans_apply(&trans, dm);
////
////
////    amxd_trans_clean(&trans);
////
//
//    amxd_dm_t* dm = agent_get_dm();
//    if(dm == NULL) {
//        SAH_TRACEZ_INFO(ME, "dm is NULL");
//    }
//
//    amxd_object_t* object = NULL;
//    object = amxd_object_findf(amxd_dm_get_root(dm), "%s", path);
//    if (object == NULL) {
//        SAH_TRACEZ_INFO(ME, "obj not NULL");
//    }
//
//    amxd_status_t status;
//    status = amxd_object_set_value(uint32_t, object, param_name, param_value);
//    if(status != amxd_status_ok) {
//        SAH_TRACEZ_ERROR(ME, "Failed to set value");
//    }
//}

bool agent_get_ahdcpmessage_enables(col_conf_t* col) {
    bool agent_enable = amxd_object_get_value(bool, col->agent_obj, "Enable", NULL);
    bool ahdsource_enable = amxd_object_get_value(bool, col->ahdsource_obj, "Enable", NULL);
    bool ahdcpmessage_enable = amxd_object_get_value(bool, col->ahdcpmessage_obj, "Enable", NULL);

    return agent_enable & ahdsource_enable & ahdcpmessage_enable;
}

// returns future timestamp if volumetry max has been reached, 0 otherwise
uint32_t msg_based_update(col_conf_t* col, uint32_t size) {
    uint32_t ts_timestamp = 0;
    char time_buffer[256];
    time_t now = time(NULL);
    strftime(time_buffer, sizeof(time_buffer), "%FT%TZ", gmtime((time_t*) &now));
    SAH_TRACEZ_INFO(ME, "Current time [%lu]: %s", (unsigned long) now, time_buffer);

    // read dm config
    int32_t max_bytes_per_sec = agent_dm_get_ahdcpmessage_config_param(col, "MaxAllowedBytesPerSecond");
    int32_t max_bytes_per_min = agent_dm_get_ahdcpmessage_config_param(col, "MaxAllowedBytesPerMinute");
    int32_t max_bytes_per_hr = agent_dm_get_ahdcpmessage_config_param(col, "MaxAllowedBytesPerHour");
    int32_t max_bytes_per_day = agent_dm_get_ahdcpmessage_config_param(col, "MaxAllowedBytesPerDay");

    if(((col->cur_bytes_per_day + size) >= max_bytes_per_day) && (max_bytes_per_day != -1)) {
        SAH_TRACEZ_INFO(ME, "Max bytes per day reached. Collector %s stopped", col->name);
        ts_timestamp = now + (86400 - (now % 86400));
    } else if(((col->cur_bytes_per_hr + size) >= max_bytes_per_hr) && (max_bytes_per_hr != -1)) {
        SAH_TRACEZ_INFO(ME, "Max bytes per hour reached. Collector %s stopped", col->name);
        ts_timestamp = now + (3600 - (now % 3600));
    } else if(((col->cur_bytes_per_min + size) >= max_bytes_per_min) && (max_bytes_per_min != -1)) {
        SAH_TRACEZ_INFO(ME, "Max bytes per min reached. Collector %s stopped", col->name);
        //SAH_TRACEZ_INFO(ME, "MaxAllowedBytesPerMin = %d, CurBytesPerMin = %d", col->max_bytes_per_min, col->cur_bytes_per_min);
        ts_timestamp = now + (60 - (now % 60)); // start of next min ts
    } else if(((col->cur_bytes_per_sec + size) >= max_bytes_per_sec) && (max_bytes_per_sec != -1)) {
        SAH_TRACEZ_INFO(ME, "Max bytes per sec reached. Collector %s stopped", col->name);
        ts_timestamp = now + 1; // start of next sec ts
    }

    if(ts_timestamp) {
        SAH_TRACEZ_INFO(ME, "Future time [%lu]", (unsigned long) ts_timestamp);
        agent_dm_update_status(col->ahdcpmessage_obj, "Stopped");
        //strftime(time_buffer, sizeof(time_buffer), "%FT%TZ", gmtime((time_t *) &ts_timestamp));
        //SAH_TRACEZ_INFO(ME, "Start of next timeslot: %s", time_buffer);
    } else {
        // update col dm ongoing ts
        col->cur_bytes_per_sec += size;
        col->cur_bytes_per_min += size;
        col->cur_bytes_per_hr += size;
        col->cur_bytes_per_day += size;
        col->cur_msg_sent_per_sec++;
        col->cur_msg_sent_per_min++;
        col->cur_msg_sent_per_hr++;
        col->cur_msg_sent_per_day++;
        agent_calculate_avg(col);
        update_dm_ongoing_ts(col);
    }
    return ts_timestamp;
}

static void update_dm_stats(col_conf_t* col, const char* ts_type, uint8_t ts_size) {
    SAH_TRACEZ_INFO(ME, "Update data model stats");

    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("DataCollect.");
    amxc_var_t ret;
    amxc_var_t req_paths;
    amxc_var_t* req_path = NULL;
    amxc_var_t* params2 = NULL;

    amxc_var_init(&req_paths);
    amxc_var_set_type(&req_paths, AMXC_VAR_ID_LIST);
    amxc_var_init(&ret);

    char path[256];
    for(int i = 0; i < ts_size; i++) {
        sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.Stats.%s.TimeSlot-%d.", col->pos, ts_type, i + 1);

        req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
        amxc_var_add_key(cstring_t, req_path, "path", path);
        params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
        if(strcmp(ts_type, "PerSecond") == 0) {
            amxc_var_add_key(uint32_t, params2, "BytesSent", col->stats_per_sec.ts[i].bytes_sent);
            amxc_var_add_key(uint32_t, params2, "MessagesSent", col->stats_per_sec.ts[i].msg_sent);
            //SAH_TRACEZ_INFO(ME, "BytesSent = %d", col->stats_per_sec.ts[i].bytes_sent);
        } else if(strcmp(ts_type, "PerMinute") == 0) {
            amxc_var_add_key(uint32_t, params2, "BytesSent", col->stats_per_min.ts[i].bytes_sent);
            amxc_var_add_key(uint32_t, params2, "MessagesSent", col->stats_per_min.ts[i].msg_sent);
            SAH_TRACEZ_INFO(ME, "BytesSent = %d", col->stats_per_min.ts[i].bytes_sent);
        } else if(strcmp(ts_type, "PerHour") == 0) {
            amxc_var_add_key(uint32_t, params2, "BytesSent", col->stats_per_hr.ts[i].bytes_sent);
            amxc_var_add_key(uint32_t, params2, "MessagesSent", col->stats_per_hr.ts[i].msg_sent);
            SAH_TRACEZ_INFO(ME, "BytesSent = %d", col->stats_per_hr.ts[i].bytes_sent);
        } else if(strcmp(ts_type, "PerDay") == 0) {
            amxc_var_add_key(uint32_t, params2, "BytesSent", col->stats_per_day.ts[i].bytes_sent);
            amxc_var_add_key(uint32_t, params2, "MessagesSent", col->stats_per_day.ts[i].msg_sent);

            amxc_var_add_key(uint32_t, params2, "MinBytesSentPerSecond", col->stats_per_day.ts[i].min_bytes_sent_persec);
            amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerSecond", col->stats_per_day.ts[i].max_bytes_sent_persec);
            amxc_var_add_key(double, params2, "AvgBytesSentPerSecond", col->stats_per_day.ts[i].avg_bytes_sec_ts);
            amxc_var_add_key(uint32_t, params2, "MinBytesSentPerMinute", col->stats_per_day.ts[i].min_bytes_sent_permin);
            amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerMinute", col->stats_per_day.ts[i].max_bytes_sent_permin);
            amxc_var_add_key(double, params2, "AvgBytesSentPerMinute", col->stats_per_day.ts[i].avg_bytes_min_ts);
            amxc_var_add_key(uint32_t, params2, "MinBytesSentPerHour", col->stats_per_day.ts[i].min_bytes_sent_perhr);
            amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerHour", col->stats_per_day.ts[i].max_bytes_sent_perhr);
            amxc_var_add_key(double, params2, "AvgBytesSentPerHour", col->stats_per_day.ts[i].avg_bytes_hr_ts);
            amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerSecond", col->stats_per_day.ts[i].min_msg_sent_persec);
            amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerSecond", col->stats_per_day.ts[i].max_msg_sent_persec);
            amxc_var_add_key(double, params2, "AvgMessagesSentPerSecond", col->stats_per_day.ts[i].avg_msg_sec_ts);
            amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerMinute", col->stats_per_day.ts[i].min_msg_sent_permin);
            amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerMinute", col->stats_per_day.ts[i].max_msg_sent_permin);
            amxc_var_add_key(double, params2, "AvgMessagesSentPerMinute", col->stats_per_day.ts[i].avg_msg_min_ts);
            amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerHour", col->stats_per_day.ts[i].min_msg_sent_perhr);
            amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerHour", col->stats_per_day.ts[i].max_msg_sent_perhr);
            amxc_var_add_key(double, params2, "AvgMessagesSentPerHour", col->stats_per_day.ts[i].avg_msg_hr_ts);
        }
    }
    if(strcmp(ts_type, "PerSecond") != 0) {   // Do not update for PerSecond Stats
        sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.Stats.%s.TimeSlot-ongoing.", col->pos, ts_type);

        req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
        amxc_var_add_key(cstring_t, req_path, "path", path);
        params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
        amxc_var_add_key(uint32_t, params2, "BytesSent", 0);
        amxc_var_add_key(uint32_t, params2, "MessagesSent", 0);
        if(strcmp(ts_type, "PerDay") == 0) {
            amxc_var_add_key(uint32_t, params2, "MinBytesSentPerSecond", 0);
            amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerSecond", 0);
            amxc_var_add_key(double, params2, "AvgBytesSentPerSecond", 0);
            amxc_var_add_key(uint32_t, params2, "MinBytesSentPerMinute", 0);
            amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerMinute", 0);
            amxc_var_add_key(double, params2, "AvgBytesSentPerMinute", 0);
            amxc_var_add_key(uint32_t, params2, "MinBytesSentPerHour", 0);
            amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerHour", 0);
            amxc_var_add_key(double, params2, "AvgBytesSentPerHour", 0);
            amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerSecond", 0);
            amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerSecond", 0);
            amxc_var_add_key(double, params2, "AvgMessagesSentPerSecond", 0);
            amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerMinute", 0);
            amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerMinute", 0);
            amxc_var_add_key(double, params2, "AvgMessagesSentPerMinute", 0);
            amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerHour", 0);
            amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerHour", 0);
            amxc_var_add_key(double, params2, "AvgMessagesSentPerHour", 0);
        }
    }

    amxb_set_multiple(bus_ctx, 0, &req_paths, &ret, 5);
    amxc_var_clean(&ret);
    amxc_var_clean(&req_paths);
}

void update_dm_ongoing_ts(col_conf_t* col) {
    SAH_TRACEZ_INFO(ME, "Update ongoing timeslot");
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("DataCollect.");
    char path[256];
    amxc_var_t ret;
    amxc_var_t req_paths;
    amxc_var_t* req_path = NULL;
    amxc_var_t* params2 = NULL;

    amxc_var_init(&req_paths);
    amxc_var_set_type(&req_paths, AMXC_VAR_ID_LIST);
    amxc_var_init(&ret);
    SAH_TRACEZ_INFO(ME, "Update collector: %s", col->name);

    sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.Stats.PerMinute.TimeSlot-ongoing.", col->pos);

    req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
    amxc_var_add_key(cstring_t, req_path, "path", path);
    params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
    amxc_var_add_key(uint32_t, params2, "BytesSent", col->cur_bytes_per_min);
    amxc_var_add_key(uint32_t, params2, "MessagesSent", col->cur_msg_sent_per_min);

    sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.Stats.PerHour.TimeSlot-ongoing.", col->pos);

    req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
    amxc_var_add_key(cstring_t, req_path, "path", path);
    params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
    amxc_var_add_key(uint32_t, params2, "BytesSent", col->cur_bytes_per_hr);
    amxc_var_add_key(uint32_t, params2, "MessagesSent", col->cur_msg_sent_per_hr);

    sprintf(path, "DataCollect.AhDSource.%d.AhDCPmessage.Stats.PerDay.TimeSlot-ongoing.", col->pos);

    req_path = amxc_var_add(amxc_htable_t, &req_paths, NULL);
    amxc_var_add_key(cstring_t, req_path, "path", path);
    params2 = amxc_var_add_key(amxc_htable_t, req_path, "oparameters", NULL);
    amxc_var_add_key(uint32_t, params2, "BytesSent", col->cur_bytes_per_day);
    amxc_var_add_key(uint32_t, params2, "MessagesSent", col->cur_msg_sent_per_day);

    //Stats analysis
    amxc_var_add_key(uint32_t, params2, "MinBytesSentPerSecond", col->min_bytes_sent_persec == UINT32_MAX ? 0 : col->min_bytes_sent_persec);
    amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerSecond", col->max_bytes_sent_persec);
    amxc_var_add_key(double, params2, "AvgBytesSentPerSecond", col->avg_bytes_sec_ts);

    amxc_var_add_key(uint32_t, params2, "MinBytesSentPerMinute", col->min_bytes_sent_permin == UINT32_MAX ? 0 : col->min_bytes_sent_permin);
    amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerMinute", col->max_bytes_sent_permin);
    amxc_var_add_key(double, params2, "AvgBytesSentPerMinute", col->avg_bytes_min_ts);

    amxc_var_add_key(uint32_t, params2, "MinBytesSentPerHour", col->min_bytes_sent_perhr == UINT32_MAX ? 0 : col->min_bytes_sent_perhr);
    amxc_var_add_key(uint32_t, params2, "MaxBytesSentPerHour", col->max_bytes_sent_perhr);
    amxc_var_add_key(double, params2, "AvgBytesSentPerHour", col->avg_bytes_hr_ts);

    amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerSecond", col->min_msg_sent_persec == UINT32_MAX ? 0 : col->min_msg_sent_persec);
    amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerSecond", col->max_msg_sent_persec);
    amxc_var_add_key(double, params2, "AvgMessagesSentPerSecond", col->avg_msg_sec_ts);

    amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerMinute", col->min_msg_sent_permin == UINT32_MAX ? 0 : col->min_msg_sent_permin);
    amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerMinute", col->max_msg_sent_permin);
    amxc_var_add_key(double, params2, "AvgMessagesSentPerMinute", col->avg_msg_min_ts);

    amxc_var_add_key(uint32_t, params2, "MinMessagesSentPerHour", col->min_msg_sent_perhr == UINT32_MAX ? 0 : col->min_msg_sent_perhr);
    amxc_var_add_key(uint32_t, params2, "MaxMessagesSentPerHour", col->max_msg_sent_perhr);
    amxc_var_add_key(double, params2, "AvgMessagesSentPerHour", col->avg_msg_hr_ts);

    amxb_set_multiple(bus_ctx, 0, &req_paths, &ret, 5);
    amxc_var_clean(&ret);
    amxc_var_clean(&req_paths);
}

// called at each new hr ts
static void agent_update_perhr_stats_analysis(col_conf_t* col) {
    if(col->cur_bytes_per_hr < col->min_bytes_sent_perhr) {
        col->min_bytes_sent_perhr = col->cur_bytes_per_hr;
    } else if(col->cur_bytes_per_hr > col->max_bytes_sent_perhr) {
        col->max_bytes_sent_perhr = col->cur_bytes_per_hr;
    }

    if(col->cur_msg_sent_per_hr < col->min_msg_sent_perhr) {
        col->min_msg_sent_perhr = col->cur_msg_sent_per_hr;
    } else if(col->cur_msg_sent_per_hr > col->max_msg_sent_perhr) {
        col->max_msg_sent_perhr = col->cur_msg_sent_per_hr;
    }
}

// called at each new min ts
static void agent_update_permin_stats_analysis(col_conf_t* col) {
    if(col->cur_bytes_per_min && (col->cur_bytes_per_min < col->min_bytes_sent_permin)) {
        col->min_bytes_sent_permin = col->cur_bytes_per_min;
    } else if(col->cur_bytes_per_min > col->max_bytes_sent_permin) {
        col->max_bytes_sent_permin = col->cur_bytes_per_min;
    }

    if(col->cur_msg_sent_per_min && (col->cur_msg_sent_per_min < col->min_msg_sent_permin)) {
        col->min_msg_sent_permin = col->cur_msg_sent_per_min;
    } else if(col->cur_msg_sent_per_min > col->max_msg_sent_permin) {
        col->max_msg_sent_permin = col->cur_msg_sent_per_min;
    }
}

// called at each new sec ts
static void agent_update_persec_stats_analysis(col_conf_t* col) {
    if(col->cur_bytes_per_sec && (col->cur_bytes_per_sec < col->min_bytes_sent_persec)) {
        col->min_bytes_sent_persec = col->cur_bytes_per_sec;
    } else if(col->cur_bytes_per_sec > col->max_bytes_sent_persec) {
        col->max_bytes_sent_persec = col->cur_bytes_per_sec;
    }

    if(col->cur_msg_sent_per_sec && (col->cur_msg_sent_per_sec < col->min_msg_sent_persec)) {
        col->min_msg_sent_persec = col->cur_msg_sent_per_sec;
    } else if(col->cur_msg_sent_per_sec > col->max_msg_sent_persec) {
        col->max_msg_sent_persec = col->cur_msg_sent_per_sec;
    }
    //agent_update_permin_stats_analysis(col);
    //agent_update_perhr_stats_analysis(col);
}

void col_update_day_stats(amxp_timer_t* timer UNUSED, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Update day stats");
    col_conf_t* col;
    amxc_llist_for_each(it, (&col_lst)) {
        col = amxc_container_of(it, col_conf_t, it);
        SAH_TRACEZ_INFO(ME, "Update collector: %s", col->name);
        agent_calculate_avg(col);
        for(int i = 9; i > 0; i--) {
            //col->stats_per_day.ts[i].bytes_sent = col->stats_per_day.ts[i - 1].bytes_sent;
            //col->stats_per_day.ts[i].msg_sent = col->stats_per_day.ts[i - 1].msg_sent;
            memcpy(&col->stats_per_day.ts[i], &col->stats_per_day.ts[i - 1], sizeof(day_ts_stats));
        }
        col->stats_per_day.ts[0].bytes_sent = col->cur_bytes_per_day;
        col->stats_per_day.ts[0].msg_sent = col->cur_msg_sent_per_day;
        col->stats_per_day.ts[0].min_bytes_sent_perhr = col->min_bytes_sent_perhr;
        col->stats_per_day.ts[0].min_bytes_sent_permin = col->min_bytes_sent_permin;
        col->stats_per_day.ts[0].min_bytes_sent_persec = col->min_bytes_sent_persec;
        col->stats_per_day.ts[0].max_bytes_sent_perhr = col->max_bytes_sent_perhr;
        col->stats_per_day.ts[0].max_bytes_sent_permin = col->max_bytes_sent_permin;
        col->stats_per_day.ts[0].max_bytes_sent_persec = col->max_bytes_sent_persec;
        col->stats_per_day.ts[0].min_msg_sent_perhr = col->min_msg_sent_perhr;
        col->stats_per_day.ts[0].min_msg_sent_permin = col->min_msg_sent_permin;
        col->stats_per_day.ts[0].min_msg_sent_persec = col->min_msg_sent_persec;
        col->stats_per_day.ts[0].max_msg_sent_perhr = col->max_msg_sent_perhr;
        col->stats_per_day.ts[0].max_msg_sent_permin = col->max_msg_sent_permin;
        col->stats_per_day.ts[0].max_msg_sent_persec = col->max_msg_sent_persec;
        col->stats_per_day.ts[0].avg_bytes_sec_ts = col->avg_bytes_sec_ts;
        col->stats_per_day.ts[0].avg_bytes_min_ts = col->avg_bytes_min_ts;
        col->stats_per_day.ts[0].avg_bytes_hr_ts = col->avg_bytes_hr_ts;
        col->stats_per_day.ts[0].avg_msg_sec_ts = col->avg_msg_sec_ts;
        col->stats_per_day.ts[0].avg_msg_min_ts = col->avg_msg_min_ts;
        col->stats_per_day.ts[0].avg_msg_hr_ts = col->avg_msg_hr_ts;

        col->cur_bytes_per_day = 0;
        col->cur_msg_sent_per_day = 0;
        update_dm_stats(col, "PerDay", 10);
        agent_reset_stats_analysis(col);
    }
}

void col_update_hr_stats(amxp_timer_t* timer UNUSED, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Update hr stats");
    col_conf_t* col;
    amxc_llist_for_each(it, (&col_lst)) {
        col = amxc_container_of(it, col_conf_t, it);
        SAH_TRACEZ_INFO(ME, "Update collector: %s", col->name);
        col->num_hr_ts++;
        agent_update_perhr_stats_analysis(col);
        for(int i = 23; i > 0; i--) {
            col->stats_per_hr.ts[i].bytes_sent = col->stats_per_hr.ts[i - 1].bytes_sent;
            col->stats_per_hr.ts[i].msg_sent = col->stats_per_hr.ts[i - 1].msg_sent;
        }
        col->stats_per_hr.ts[0].bytes_sent = col->cur_bytes_per_hr;
        col->stats_per_hr.ts[0].msg_sent = col->cur_msg_sent_per_hr;
        col->cur_bytes_per_hr = 0;
        col->cur_msg_sent_per_hr = 0;
        update_dm_stats(col, "PerHour", 24);
    }
}

void col_update_min_stats(amxp_timer_t* timer UNUSED, void* priv UNUSED) {
    SAH_TRACEZ_INFO(ME, "Update min stats");
    col_conf_t* col;
    amxc_llist_for_each(it, (&col_lst)) {
        col = amxc_container_of(it, col_conf_t, it);
        SAH_TRACEZ_INFO(ME, "Update collector: %s", col->name);
        col->num_min_ts++;
        agent_update_permin_stats_analysis(col);
        for(int i = 9; i > 0; i--) {
            col->stats_per_min.ts[i].bytes_sent = col->stats_per_min.ts[i - 1].bytes_sent;
            col->stats_per_min.ts[i].msg_sent = col->stats_per_min.ts[i - 1].msg_sent;
        }
        col->stats_per_min.ts[0].bytes_sent = col->cur_bytes_per_min;
        col->stats_per_min.ts[0].msg_sent = col->cur_msg_sent_per_min;
        col->cur_bytes_per_min = 0;
        col->cur_msg_sent_per_min = 0;
        update_dm_stats(col, "PerMinute", 10);
    }
}

void col_update_sec_stats(amxp_timer_t* timer UNUSED, void* priv UNUSED) {
    //SAH_TRACEZ_INFO(ME, "Update sec stats");
    col_conf_t* col;
    amxc_llist_for_each(it, (&col_lst)) {
        col = amxc_container_of(it, col_conf_t, it);
        SAH_TRACEZ_INFO(ME, "Update collector: %s", col->name);
        col->num_sec_ts++; //to be used for calculating average
        agent_update_persec_stats_analysis(col);
        for(int i = 4; i > 0; i--) {
            col->stats_per_sec.ts[i].bytes_sent = col->stats_per_sec.ts[i - 1].bytes_sent;
            col->stats_per_sec.ts[i].msg_sent = col->stats_per_sec.ts[i - 1].msg_sent;
        }
        col->stats_per_sec.ts[0].bytes_sent = col->cur_bytes_per_sec;
        col->stats_per_sec.ts[0].msg_sent = col->cur_msg_sent_per_sec;
        update_dm_stats(col, "PerSecond", 5);
        col->cur_bytes_per_sec = 0;
        col->cur_msg_sent_per_sec = 0;
    }
}
